// Compile with:
// gcc gtkversiontest.c -o gtkversiontest `pkg-config --cflags --libs gtk+-3.0`

#include <gtk/gtk.h>

int main(int argc, char **argv) 
{

    GtkApplication *application;
    int status;
    
    application = gtk_application_new (NULL, G_APPLICATION_NON_UNIQUE);
    //g_application_set_application_id (G_APPLICATION(application), "org.gtk3.notification");
    g_application_register (G_APPLICATION(application), NULL, NULL);
    

#if GTK_CHECK_VERSION(3, 14, 0)
    printf(">=3.14.0\n");
    
    GNotification *notification;
    notification = g_notification_new ("Hello Liefie");
    
    g_application_send_notification (G_APPLICATION(application), "hello", notification);
    g_object_unref (notification);

#elif GTK_CHECK_VERSION(2, 10, 0)
    printf(">=2.10.0\n");
        
#else
    printf("<2.10.0\n");
    
#endif
        
    return 0;
}
