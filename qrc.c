// QR Code Generator in ansi c using the IUP library

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iup.h>

#include "qrcgen.c"

//extern const char *QRModeString []; //defined in qrcgen.c

// I need access to certain gui widgets from callbacks
// and I cannot work out how to avoid using global vars.
// At least static restricts them to this file scope.
static Ihandle *ibut_up, *ibut_in, *ibut_dn;
static Ihandle *numchars, *encoding;
static Ihandle *qrcbox; 

// --------------------------
// Icons
// --------------------------
static unsigned char BLANK_ICON [] =
{6,6,6,6,6,1,1,1,1,1,1,6,6,6,6,6
,6,1,1,1,6,1,1,1,1,1,1,6,1,1,1,6
,6,1,6,1,6,1,1,1,1,1,1,6,1,6,1,6
,6,1,1,1,6,1,1,1,1,1,1,6,1,1,1,6
,6,6,6,6,6,1,1,1,1,1,1,6,6,6,6,6
,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
,6,6,6,6,6,1,1,1,1,1,6,6,6,1,1,1
,6,1,1,1,6,1,1,1,1,1,6,1,6,1,1,1
,6,1,6,1,6,1,1,1,1,1,6,6,6,1,1,1
,6,1,1,1,6,1,1,1,1,1,1,1,1,1,1,1
,6,6,6,6,6,1,1,1,1,1,1,1,1,1,1,1
};

static unsigned char QRC_ICON [] =
{6,6,6,6,6,1,6,1,6,6,1,6,6,6,6,6
,6,1,1,1,6,1,1,6,1,6,1,6,1,1,1,6
,6,1,6,1,6,1,6,1,6,1,1,6,1,6,1,6
,6,1,1,1,6,1,1,1,6,6,1,6,1,1,1,6
,6,6,6,6,6,1,1,6,1,6,1,6,6,6,6,6
,1,1,1,1,1,1,6,6,1,1,1,1,1,1,1,1
,6,1,6,1,6,1,6,1,6,6,1,6,1,6,6,1
,1,6,6,6,1,1,6,1,6,1,6,1,6,6,1,6
,6,1,6,1,6,1,6,6,1,6,1,1,6,1,6,6
,1,6,1,6,1,6,1,6,1,6,1,1,6,6,1,6
,1,1,1,1,1,1,1,6,6,1,1,1,1,1,1,1
,6,6,6,6,6,1,6,1,1,1,6,6,6,1,6,6
,6,1,1,1,6,1,6,6,6,1,6,1,6,1,1,6
,6,1,6,1,6,1,6,1,6,1,6,6,6,1,6,6
,6,1,1,1,6,1,6,6,1,1,1,1,1,1,6,1
,6,6,6,6,6,1,1,6,6,1,6,1,6,6,6,6
};

static unsigned char IBUT_ICON [] =
{1,4,4,4,4,4,4,4,4,4,4,4,4,4,4,1
,4,4,2,2,2,2,2,2,2,2,2,2,2,2,4,4
,4,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4
,4,2,2,1,1,1,1,1,1,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,6,6,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,6,6,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,1,1,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,6,6,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,6,6,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,6,6,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,6,6,1,1,1,1,2,2,4
,4,2,2,1,1,1,1,6,6,6,6,1,1,2,2,4
,4,2,2,1,1,1,1,1,6,6,1,1,1,2,2,4
,4,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4
,4,4,2,2,2,2,2,2,2,2,2,2,2,2,4,4
,1,4,4,4,4,4,4,4,4,4,4,4,4,4,4,1
};

// --------------------------
// Callbacks
// --------------------------

int quit_cb(void)
{
    return IUP_CLOSE;
};

// --------------------------
// On change to txtbox contents
// --------------------------
int txtbox_cb(Ihandle *ih)
{
    // Update numchars
    static int nc;
    nc= IupGetInt(ih,"COUNT");
    IupSetInt(numchars,"TITLE",nc);
    
    // Get txtbox contents
    static char *str;
    str = IupGetAttribute(ih,"VALUE");
    
    // Set encoding
    IupSetStrAttribute(encoding,"TITLE", QRModeString[getQRMode(str)] ); 

    // Update qrcbox
    static unsigned char *qrc;
    static int len;
    len = qrcgen(str, &qrc);

    if(len == 0) {
        Ihandle *img = IupImage(16,16,BLANK_ICON);
        IupSetAttribute(img,"1"," 42  42  42");
        IupSetAttribute(img,"6","255 255 255");
        IupSetAttributeHandle(qrcbox, "IMAGE", img);

    } else {
        int w = (int)sqrt( (float)len );
        int h = len / w;

        printf("Update qrcbox len: %d, w: %d, h: %d\n",len,w,h);
        
        Ihandle *img = IupImage(w,h,qrc);
        IupSetAttribute(img,"0"," 42  42  42");
        IupSetAttribute(img,"1","255 255 255");
        IupSetAttributeHandle(qrcbox, "IMAGE", img);
    };

    return 0;
};

// --------------------------
// On change to resize slider
// --------------------------
int resize_cb(Ihandle *ih)
{
    int sz = IupGetInt(ih,"VALUE");
    printf("Resizing to: %d\n",sz);
    // Resize the image !!
    //
    //
    //
    return 0;
};

// --------------------------
// On select template from dropdown
// --------------------------
int tmplst_cb( Ihandle *ih, char *text, int item, int state)
{
    //printf("ih: %d, text: %s, item: %d, state: %d\n", ih, text, item, state);

    if ( (item == 4) && (state == 1) ) // Add from textbox
    {
        IupSetAttribute(ih, "APPENDITEM", "New item");
    }
};

// --------------------------
// i Button
// --------------------------
typedef enum {
    BUT_INIT, 
    BUT_ENTER, 
    BUT_CLICKED, 
    BUT_LEAVE, 
    BUT_NUMSTATES
} butstate_t;

butstate_t butstate = BUT_INIT;

int ibut_button(Ihandle *ih, int button, int pressed, int x, int y, char* status)
{
    if( butstate == BUT_ENTER )
    {
        IupSetAttributeHandle(ih,"IMAGE",ibut_dn);
        if ( !pressed )
        {
            printf("released button: %d\n", button);
        
            if( button == IUP_BUTTON1 ) 
            {
                butstate = BUT_CLICKED;
                printf("do something while butstate: %d\n", butstate);
                butstate = BUT_ENTER;
            };
            IupSetAttributeHandle(ih,"IMAGE",ibut_in);
        };
    };
    return 0;
};

int ibut_enter(Ihandle *ih)
{
    printf("Entered\n");
    butstate = BUT_ENTER;
    IupSetAttributeHandle(ih,"IMAGE",ibut_in);
    return 0;
};

int ibut_leave(Ihandle *ih)
{
    printf("Leaving\n");
    butstate = BUT_LEAVE;
    IupSetAttributeHandle(ih,"IMAGE",ibut_up);
    return 0;
};


// --------------------------
// Main
// --------------------------
int main (int argc, char** argv) 
{

    printf("getVersion: %d\n", getVersion('M',2,200) );

    Ihandle *dlg, *mainbox, *resize;
    Ihandle *statline, *tmplst, *txtbox, *ibut;
    Ihandle *quit_bt;

    IupOpen(&argc, &argv);

    quit_bt = IupButton("Quit", 0);
    IupSetCallback(quit_bt, "ACTION", (Icallback)quit_cb);

    // Textbox pane
    txtbox = IupText("TXTACTION");
    IupSetAttribute(txtbox,"MARGIN",         "2x2");
    IupSetAttribute(txtbox,"MULTILINE",      "Yes");
    IupSetAttribute(txtbox,"VISIBLECOLUMNS", "20");
    IupSetAttribute(txtbox,"VISIBLELINES",   "14");
    IupSetAttribute(txtbox,"EXPAND",         "Yes");

    IupSetCallback(txtbox,"VALUECHANGED_CB",txtbox_cb);

    // QR Code pane
    qrcbox = IupLabel(0);
    Ihandle *qrcboximg = IupImage(16,16,BLANK_ICON);
    IupSetAttribute(qrcboximg,"0","  0   0   0");
    IupSetAttribute(qrcboximg,"1"," 42  42  42");
    IupSetAttribute(qrcboximg,"2"," 84  84  84");
    IupSetAttribute(qrcboximg,"3","126 126 126");
    IupSetAttribute(qrcboximg,"4","168 168 168");
    IupSetAttribute(qrcboximg,"5","210 210 210");
    IupSetAttribute(qrcboximg,"6","255 255 255");

    IupSetAttributeHandle(qrcbox,"IMAGE",qrcboximg);
    IupSetAttribute(qrcbox,"EXPAND","Yes");
    IupSetAttribute(qrcbox,"ALIGNMENT","ACENTER:ACENTER");

    // Resize scrollbar
    resize = IupVal("HORIZONTAL");
    IupSetAttribute(resize,"MAX","100");
    IupSetAttribute(resize,"STEP","0.1");
    IupSetAttribute(resize,"SIZE","100x10");
    IupSetAttribute(resize,"EXPAND","HORIZONTAL");
    IupSetAttribute(resize,"VALUE","50");
    IupSetCallback(resize,"VALUECHANGED_CB",(Icallback)resize_cb);


    // Template dropdown
    tmplst = IupList("TEMPLATEACTION");
    IupSetAttribute(tmplst,"DROPDOWN","Yes");
    
    IupSetAttribute(tmplst,"1","None");
    IupSetAttribute(tmplst,"2","V-Card");
    IupSetAttribute(tmplst,"3","Other");
    IupSetAttribute(tmplst,"4","<Add from textbox>");
    IupSetAttribute(tmplst,"VALUE","1");

    IupSetCallback(tmplst,"ACTION",(Icallback)tmplst_cb);

    // i Button
    ibut = IupLabel(0);
    IupSetCallback(ibut, "BUTTON_CB", (Icallback)ibut_button);
    IupSetCallback(ibut, "ENTERWINDOW_CB", (Icallback)ibut_enter);
    IupSetCallback(ibut, "LEAVEWINDOW_CB", (Icallback)ibut_leave);
    
    ibut_up = IupImage(16,16,IBUT_ICON);
    IupSetAttribute(ibut_up,"0","  0   0   0");
    IupSetAttribute(ibut_up,"1"," 42  42  42");
    IupSetAttribute(ibut_up,"2"," 42  42  42");
    IupSetAttribute(ibut_up,"3"," 42  42  42");
    IupSetAttribute(ibut_up,"4","168 168 168");
    IupSetAttribute(ibut_up,"5"," 42  42  42");
    IupSetAttribute(ibut_up,"6","210 210 210");
    
    ibut_in = IupImage(16,16,IBUT_ICON);
    IupSetAttribute(ibut_in,"0","  0   0   0");
    IupSetAttribute(ibut_in,"1"," 42  42  42");
    IupSetAttribute(ibut_in,"2"," 42  84  84");
    IupSetAttribute(ibut_in,"3"," 42 126 126");
    IupSetAttribute(ibut_in,"4"," 42 168 168");
    IupSetAttribute(ibut_in,"5"," 42 210 210");
    IupSetAttribute(ibut_in,"6"," 42 255 255");
    
    ibut_dn = IupImage(16,16,IBUT_ICON);
    IupSetAttribute(ibut_dn,"0","  0   0   0");
    IupSetAttribute(ibut_dn,"1"," 42  42  42");
    IupSetAttribute(ibut_dn,"2"," 84  84  42");
    IupSetAttribute(ibut_dn,"3","126 126  42");
    IupSetAttribute(ibut_dn,"4","168 168  42");
    IupSetAttribute(ibut_dn,"5","210 210  42");
    IupSetAttribute(ibut_dn,"6","255 255  42");

    IupSetAttributeHandle(ibut,"IMAGE",ibut_up);

    // Statusline
    numchars = IupLabel("0");
    IupSetAttribute(numchars,"SIZE","16x8");
    IupSetAttribute(numchars,"ALIGNMENT","ARIGHT:ACENTER");
    
    encoding = IupLabel( QRModeString[getQRMode("")] );
    IupSetAttribute(encoding,"SIZE","80x8");
    
    statline= IupHbox
        ( tmplst
        , IupLabel("  Chars:")
        , numchars
        , IupLabel("  Encoding:")
        , encoding
        , IupFill()
        , IupButton("Export","EXPORT_CB") // Make it look like the i button!
        , ibut
        , 0);
    IupSetAttribute(statline,"ALIGNMENT","ACENTER");

    // Main Vbox
    printf("Got to mainbox\n");
    mainbox = IupVbox( 
        IupHbox
            ( txtbox
            , IupVbox
                ( qrcbox
                , IupFill()
                , resize
                , 0)
            , 0 )
        , statline
        , 0 );

    IupSetAttribute(mainbox, "MARGIN", "2x2");
    IupSetAttribute(mainbox, "GAP", "2");

    // The Dialog
    printf("Got to dialog\n");
    dlg = IupDialog(mainbox);
    IupSetAttribute(dlg, "TITLE", "QR Code Generator");
    IupSetAttributeHandle(dlg, "DEFAULTESC", quit_bt);
    IupSetAttribute(dlg, "BGCOLOR", " 42  42  42");
    IupSetAttribute(dlg, "FGCOLOR", "210 210 210");
    IupSetAttribute(dlg, "TITLE", "QR Code Generator");

    Ihandle *icon = IupImage(16,16,QRC_ICON);
    IupSetAttribute(icon,"0","  0   0   0");
    IupSetAttribute(icon,"1"," 42  42  42");
    IupSetAttribute(icon,"2"," 84  84  84");
    IupSetAttribute(icon,"3","126 126 126");
    IupSetAttribute(icon,"4","168 168 168");
    IupSetAttribute(icon,"5","210 210 210");
    IupSetAttribute(icon,"6","255 255 255");
    IupSetAttributeHandle(dlg,"ICON",icon);
        
    printf("Got to show\n");
    IupShow(dlg);

    printf("Got to mainloop\n");
    IupMainLoop();

    printf("Got to destroy/close\n");
    IupDestroy(dlg);
    IupClose();

    printf("Got to end of main\n");
    return 0;

};
