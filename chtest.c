#include <stdio.h>

char *flip(const char *str) 
{
    int len = strlen(str);
    char *flp = (char *)malloc(len);
    int i;
    for(i=(len-1); i>=0; i--) {
        flp[i] = *(str++);
    };

    printf("flip %s ptr: %d\n", flp, &flp);

    return flp;
};

int warp(char **strp)
{
    char *str = *strp;
    int len = strlen( str );
    char *wrp = (char *)malloc(len);
    
    int i;
    for(i=0; i<len; i++) {
        wrp[i] = *(str++) + 1;
    };

    printf("warp %s ptr: %d\n", wrp, &wrp);

    *strp = wrp;
    return len;

};

typedef struct pix_t {
    int len;
    char *vec;
} pix_t;

pix_t *pixy(char *str)
{
    pix_t pix = (pix_t)malloc( sizeof(pix_t) );
    pix.len = strlen(str);
    pix.vec = (char *)malloc(pix.len);

    int i;
    for(i=0; i<pix.len; i++) {
        pix.vec[i] = *(str++) - 2;
    };

    printf("pixy %s ptr: %d\n", pix.vec, &(pix.vec));

    return &pix;
};

int main()
{
    char *str = "Hello, World!";
    printf("str: %s ptr: %d\n", str, &str);

    char *flp = flip(str);
    printf("flp: %s ptr: %d\n", flp, &flp);

    int len = warp(&str);
    printf("wrp: %s ptr: %d\n", str, &str);

    pix_t *pix = pixy(str);
    printf("pix: %s ptr: %d\n", (*pix).vec, &((*pix).vec) );

    return 0;
}
