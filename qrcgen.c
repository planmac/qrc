// -------------------------------------------------------------------
// Generate a QR Code from a source string
// -------------------------------------------------------------------
//
// 1. Data Analysis
//    -------------
//    Determine which mode can be applied:
//      Numeric: [0-9]
//      Alphanumeric: [0-9] | [A-Z] | [$,%,*,+,-,.,/,:, ]
//      Byte: ISO-8859-1 character set
//
//    This implementation will assume byte if numeric and alphanumeric fail.
//
// 2. Data Encoding
//    -------------
//    Convert text to a string of bits split into 8 bit codewords:
//    1. Choose error correction level: L, M, Q or H
//    2. Determine smallest Version for the data, from the table
//
// 3. Error Correction Encoding
//    -------------------------
//    Generate Reed-Solomon error correction codewords
//
// 4. Structure Final Message
//    -----------------------
//    Interleave the data and error correction codewords into data block
//
// 5. Module Placement in Matrix
//    --------------------------
//    Place the common patterns and the data+error codewords
//
// 6. Data Masking
//    ------------
//    Determine which of the eight mask patterns results in the lowest penalty score
//    Apply the chosen mask to the matrix
//
// 7. Format and Version Information
//    ------------------------------
//    Insert the format and version information into the matrix
//
// ------------------------------------------------------------------

#include <string.h>
#include <stdlib.h>

// Forward defs for this file
typedef unsigned char *qrc_t;
int qrcgen(static char *str, qrc_t *qrcref);

// --------------------------
// Choose error correction level
// --------------------------
char getECL(char *str)
{
    // L - Recovers 7% of data
    // M - 15%
    // Q - 25%
    // H - 40

    return 'Q';
}

// --------------------------
// Determine QR Mode
// --------------------------

// NUMERIC_MODE  0001
// ALPHANUM_MODE 0010
// BYTE_MODE     0100
// KANJI_MODE    1000 // Not implemented
// ECI_MODE      0111 // Not implemented

const char *QRModeString [] = 
{  "Undetermined", "Numeric", "Alphanumeric", "", "Byte"    };
//  0 0b0000        1 0b0001   2 0b0010       3    4 0b0100

#define between(lo, hi, ch) ( ( (lo) <= (ch) ) && ( (ch) <= (hi) ) )
#define MAX2(a,b)     ( ((a)>(b)) ? (a) : (b) )
//#define MAX3(a,b,c)   MAX2( MAX2((a),(b)),(c) )
#define MAX4(a,b,c,d) MAX2( MAX2((a),(b)), MAX2((c),(d)) )

int getQRMode(const char *str)
{
 
    // If str is empty return Undefined
    if(*str == '\0')
        return 0;

    int num = 0;
    int alp = 0;
    int byt = 0;
    int res = 0;

    do {
        num = between('0','9',*str);
        
        alp = (
            between('A','Z',*str)
            || strchr("$%*+-./: ",*str)
        ) * 2;

        // Assume byte if not numeric or alphanumeric
        byt = !(num || alp) * 4;
    
        res = MAX4(res, byt, alp, num); 

      // Loop until end of string or hit "byte" character
    } while( *(++str) != '\0' && (res != 4) );

    return res;
};

// --------------------------
// Determine best QR Version 
// --------------------------

// Character capacities by version, error correction level and mode
typedef struct charcap_line {
    int     ver; // QR Version
    char    ecl; // Error Correction Level
    int     num; // Max chars for numeric
    int     alp; // Max chars for alphanumeric
    int     byt; // Max chars for byte
} charcap_line;

static charcap_line CHARCAP [] = 
{ { 1, 'L', 41, 25, 17}
, { 1, 'M', 34, 20, 14}
, { 1, 'Q', 27, 16, 11}
, { 1, 'H', 17, 10, 7}
, { 2, 'L', 77, 47, 32}
, { 2, 'M', 63, 38, 26}
, { 2, 'Q', 48, 29, 20}
, { 2, 'H', 34, 20, 14}
, { 3, 'L', 127, 77, 53}
, { 3, 'M', 101, 61, 42}
, { 3, 'Q', 77, 47, 32}
, { 3, 'H', 58, 35, 24}
, { 4, 'L', 187, 114, 78}
, { 4, 'M', 149, 90, 62}
, { 4, 'Q', 111, 67, 46}
, { 4, 'H', 82, 50, 34}
, { 5, 'L', 255, 154, 106}
, { 5, 'M', 202, 122, 84}
, { 5, 'Q', 144, 87, 60}
, { 5, 'H', 106, 64, 44}
, { 6, 'L', 322, 195, 134}
, { 6, 'M', 255, 154, 106}
, { 6, 'Q', 178, 108, 74}
, { 6, 'H', 139, 84, 58}
, { 7, 'L', 370, 224, 154}
, { 7, 'M', 293, 178, 122}
, { 7, 'Q', 207, 125, 86}
, { 7, 'H', 154, 93, 64}
, { 8, 'L', 461, 279, 192}
, { 8, 'M', 365, 221, 152}
, { 8, 'Q', 259, 157, 108}
, { 8, 'H', 202, 122, 84}
, { 9, 'L', 552, 335, 230}
, { 9, 'M', 432, 262, 180}
, { 9, 'Q', 312, 189, 130}
, { 9, 'H', 235, 143, 98}
, { 10, 'L', 652, 395, 271}
, { 10, 'M', 513, 311, 213}
, { 10, 'Q', 364, 221, 151}
, { 10, 'H', 288, 174, 119}
, { 11, 'L', 772, 468, 321}
, { 11, 'M', 604, 366, 251}
, { 11, 'Q', 427, 259, 177}
, { 11, 'H', 331, 200, 137}
, { 12, 'L', 883, 535, 367}
, { 12, 'M', 691, 419, 287}
, { 12, 'Q', 489, 296, 203}
, { 12, 'H', 374, 227, 155}
, { 13, 'L', 1022, 619, 425}
, { 13, 'M', 796, 483, 331}
, { 13, 'Q', 580, 352, 241}
, { 13, 'H', 427, 259, 177}
, { 14, 'L', 1101, 667, 458}
, { 14, 'M', 871, 528, 362}
, { 14, 'Q', 621, 376, 258}
, { 14, 'H', 468, 283, 194}
, { 15, 'L', 1250, 758, 520}
, { 15, 'M', 991, 600, 412}
, { 15, 'Q', 703, 426, 292}
, { 15, 'H', 530, 321, 220}
, { 16, 'L', 1408, 854, 586}
, { 16, 'M', 1082, 656, 450}
, { 16, 'Q', 775, 470, 322}
, { 16, 'H', 602, 365, 250}
, { 17, 'L', 1548, 938, 644}
, { 17, 'M', 1212, 734, 504}
, { 17, 'Q', 876, 531, 364}
, { 17, 'H', 674, 408, 280}
, { 18, 'L', 1725, 1046, 718}
, { 18, 'M', 1346, 816, 560}
, { 18, 'Q', 948, 574, 394}
, { 18, 'H', 746, 452, 310}
, { 19, 'L', 1903, 1153, 792}
, { 19, 'M', 1500, 909, 624}
, { 19, 'Q', 1063, 644, 442}
, { 19, 'H', 813, 493, 338}
, { 20, 'L', 2061, 1249, 858}
, { 20, 'M', 1600, 970, 666}
, { 20, 'Q', 1159, 702, 482}
, { 20, 'H', 919, 557, 382}
, { 21, 'L', 2232, 1352, 929}
, { 21, 'M', 1708, 1035, 711}
, { 21, 'Q', 1224, 742, 509}
, { 21, 'H', 969, 587, 403}
, { 22, 'L', 2409, 1460, 1003}
, { 22, 'M', 1872, 1134, 779}
, { 22, 'Q', 1358, 823, 565}
, { 22, 'H', 1056, 640, 439}
, { 23, 'L', 2620, 1588, 1091}
, { 23, 'M', 2059, 1248, 857}
, { 23, 'Q', 1468, 890, 611}
, { 23, 'H', 1108, 672, 461}
, { 24, 'L', 2812, 1704, 1171}
, { 24, 'M', 2188, 1326, 911}
, { 24, 'Q', 1588, 963, 661}
, { 24, 'H', 1228, 744, 511}
, { 25, 'L', 3057, 1853, 1273}
, { 25, 'M', 2395, 1451, 997}
, { 25, 'Q', 1718, 1041, 715}
, { 25, 'H', 1286, 779, 535}
, { 26, 'L', 3283, 1990, 1367}
, { 26, 'M', 2544, 1542, 1059}
, { 26, 'Q', 1804, 1094, 751}
, { 26, 'H', 1425, 864, 593}
, { 27, 'L', 3517, 2132, 1465}
, { 27, 'M', 2701, 1637, 1125}
, { 27, 'Q', 1933, 1172, 805}
, { 27, 'H', 1501, 910, 625}
, { 28, 'L', 3669, 2223, 1528}
, { 28, 'M', 2857, 1732, 1190}
, { 28, 'Q', 2085, 1263, 868}
, { 28, 'H', 1581, 958, 658}
, { 29, 'L', 3909, 2369, 1628}
, { 29, 'M', 3035, 1839, 1264}
, { 29, 'Q', 2181, 1322, 908}
, { 29, 'H', 1677, 1016, 698}
, { 30, 'L', 4158, 2520, 1732}
, { 30, 'M', 3289, 1994, 1370}
, { 30, 'Q', 2358, 1429, 982}
, { 30, 'H', 1782, 1080, 742}
, { 31, 'L', 4417, 2677, 1840}
, { 31, 'M', 3486, 2113, 1452}
, { 31, 'Q', 2473, 1499, 1030}
, { 31, 'H', 1897, 1150, 790}
, { 32, 'L', 4686, 2840, 1952}
, { 32, 'M', 3693, 2238, 1538}
, { 32, 'Q', 2670, 1618, 1112}
, { 32, 'H', 2022, 1226, 842}
, { 33, 'L', 4965, 3009, 2068}
, { 33, 'M', 3909, 2369, 1628}
, { 33, 'Q', 2805, 1700, 1168}
, { 33, 'H', 2157, 1307, 898}
, { 34, 'L', 5253, 3183, 2188}
, { 34, 'M', 4134, 2506, 1722}
, { 34, 'Q', 2949, 1787, 1228}
, { 34, 'H', 2301, 1394, 958}
, { 35, 'L', 5529, 3351, 2303}
, { 35, 'M', 4343, 2632, 1809}
, { 35, 'Q', 3081, 1867, 1283}
, { 35, 'H', 2361, 1431, 983}
, { 36, 'L', 5836, 3537, 2431}
, { 36, 'M', 4588, 2780, 1911}
, { 36, 'Q', 3244, 1966, 1351}
, { 36, 'H', 2524, 1530, 1051}
, { 37, 'L', 6153, 3729, 2563}
, { 37, 'M', 4775, 2894, 1989}
, { 37, 'Q', 3417, 2071, 1423}
, { 37, 'H', 2625, 1591, 1093}
, { 38, 'L', 6479, 3927, 2699}
, { 38, 'M', 5039, 3054, 2099}
, { 38, 'Q', 3599, 2181, 1499}
, { 38, 'H', 2735, 1658, 1139}
, { 39, 'L', 6743, 4087, 2809}
, { 39, 'M', 5313, 3220, 2213}
, { 39, 'Q', 3791, 2298, 1579}
, { 39, 'H', 2927, 1774, 1219}
, { 40, 'L', 7089, 4296, 2953}
, { 40, 'M', 5596, 3391, 2331}
, { 40, 'Q', 3993, 2420, 1663}
, { 40, 'H', 3057, 1852, 1273}
, {  0, '\0', 0, 0, 0}
};

int getVersion( const char ecl, const int enc, const int nchars )
{
    int ver = 0;
    int val = 0;
    charcap_line *cclp = CHARCAP;
    
    do {

        if( (*cclp).ecl == ecl )
        {
            switch (enc) {
            case 1:
                val = (*cclp).num;
                break;
            case 2:
                val = (*cclp).alp;
                break;
            case 3:
                val = (*cclp).byt;
                break;
            default:
                return 0; // Invalid encoding

            };
            if( val >= nchars )
                return (*cclp).ver;
        }

    } while ( (*(++cclp)).ver != 0 );

    return 0; // Did not find a match
};

// --------------------------
// Expand image by n units
// --------------------------

char *expandImage(const int x, const int y, const char *str, const int n)
{
    //Do nothing if params not valid
    if( *str=='\0' || n<1 || n>9 )
        return (char *)str;

    int len = strlen(str);
    if( len != (x*y) )
        return (char *)str;

    char *newstr = (char *)malloc(len*n +1);
    char *lin = (char *)malloc(x*n +1);
    char *linp;

    //Repeat for each line
    int yy, xx, nn;
    for(yy=0 ; yy<y ; yy++) {

        //Repeat for each char in line
        for(xx=0 ; xx<x ; xx++) {
            
            //(Re)set line counter
            linp = lin; 
        
            //Repeat n times for each char
            for(nn=0 ; nn<n ; nn++) {
                *(linp++) = *str;
            };

            //Advance one char
            str++;
        };

        // Null terminate the line
        *linp = '\0';

        //Add n of these lines to the newstr
        for(nn=0 ; nn<n ; nn++) {
            strcat(newstr, lin);
        };

    // Final check that all went ok
    if( strlen(newstr) != (len*n) )
        return (char *)str;
    };

    free(lin); //Avoid memory leaks! Caller must handle newstr
    return newstr;
};

// --------------------------
// Generate qr code from string
// --------------------------
// Take a string and a reference to a variable of type qrc_t, update 
// the reference parameter to point to the qrc code variable and return 
// the size (in number of modules) of the qrc code generated.

const char *bit_rep[16] = {
    [ 0] = "0000", [ 1] = "0001", [ 2] = "0010", [ 3] = "0011",
    [ 4] = "0100", [ 5] = "0101", [ 6] = "0110", [ 7] = "0111",
    [ 8] = "1000", [ 9] = "1001", [10] = "1010", [11] = "1011",
    [12] = "1100", [13] = "1101", [14] = "1110", [15] = "1111",
};

void print_byte(uint8_t byte)
{
    printf("%s%s", bit_rep[byte >> 4], bit_rep[byte & 0x0F]);
}

<<<<<<< HEAD
const char *bit_rep[16] = {
    [ 0] = "0000", [ 1] = "0001", [ 2] = "0010", [ 3] = "0011",
    [ 4] = "0100", [ 5] = "0101", [ 6] = "0110", [ 7] = "0111",
    [ 8] = "1000", [ 9] = "1001", [10] = "1010", [11] = "1011",
    [12] = "1100", [13] = "1101", [14] = "1110", [15] = "1111",
};

/* qrcgen - Generate a qr code from a string. 
 * Takes a string and a reference to a variable of type qrc_t, updates 
 * the reference parameter to point to the qrc code variable and returns 
 * the size (in number of modules) of the qrc code.*/
int qrcgen(char *str, qrc_t *qrcref)
=======
int qrcgen(static char *str, qrc_t *qrcref)
>>>>>>> 3c8b5e0ad55f8b5432b2eaa06f763572bb01ed86
{
    if(*str == '\0')
        return 0;

    // Get some stats
    char ecl = getECL(str);
    int  qrm = getQRMode(str);
    
    int slen = strlen(str);
    int ver = getVersion(ecl, qrm, slen);

    int bits = 8; //Assumed 
    int qlen = slen * bits;

<<<<<<< HEAD
    // Alloc memory for qr code string
=======
    // Allocate memory for the QR Code
>>>>>>> 3c8b5e0ad55f8b5432b2eaa06f763572bb01ed86
    qrc_t qrc = (qrc_t)malloc(qlen);
    if(!qrc)
        return 0;

<<<<<<< HEAD
    // Convert to binary & append to qrc
    char *spt = str;
    do {
        strncat(qrc, bit_rep[*spt >> 4],   4);
        strncat(qrc, bit_rep[*spt & 0x0F], 4);
    } while ( *(++spt) != '\0' );
=======
    // Convert chars to binary and append to qrc
    char *spt = str;
    do {
        printf("binary: %s%s\n", bit_rep[*spt >> 4], bit_rep[*spt & 0x0F] );
        
        strncat(qrc, bit_rep[*spt >> 4],   4);
        strncat(qrc, bit_rep[*spt & 0x0F], 4);
        printf("so far: %s\n", qrc);

    } while( *(++spt) != '\0' );
>>>>>>> 3c8b5e0ad55f8b5432b2eaa06f763572bb01ed86

    // Set reference and return length
    *qrcref = qrc;
    return qlen;
};

